import TexturesManager from "../TexturesManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Tile extends cc.Component {
  @property(TexturesManager)
  private texturesManager: TexturesManager = null;

  async onLoad(): Promise<void> {
    if(this.texturesManager != null)
      return;

    this.texturesManager = cc.find("Textures").getComponent(TexturesManager);
  }

  async resetInEditor(): Promise<void> {
    this.texturesManager = cc.find("Textures").getComponent(TexturesManager);
    this.setRandom();
  }

  setTile(index: number): void {
    this.node.getComponent(cc.Sprite).spriteFrame = this.texturesManager.getTextures()[index];
  }

  setRandom(): void {
    const randomIndex = Math.floor(Math.random() * this.texturesManager.getTexturesQnty());
    this.setTile(randomIndex);
  }
}
