import Machine from "./slots/Machine";
import TexturesManager from "./TexturesManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameManager extends cc.Component {
  @property(Machine)
  machine = null;

  @property(TexturesManager)
  texturesManager = null;

  @property({ type: cc.AudioClip })
  audioClick = null;

  private block = false;

  private result = null;

  start(): void {
    this.machine.createMachine();
  }

  update(): void {
    if (this.block && this.result != null) {
      this.informStop();
      this.result = null;
    }
  }

  click(): void {
    cc.audioEngine.playEffect(this.audioClick, false);

    if (this.machine.spinning === false) {
      this.block = false;
      this.machine.spin();
      this.requestResult();
    } else if (!this.block) {
      this.block = true;
      this.machine.lock();
    }
  }

  async requestResult(): Promise<void> {
    this.result = null;
    this.result = await this.getAnswer();
  }

  getAnswer(): Promise<Array<Array<number>>> {
    const slotResult = this.shufflePatterns();
    return new Promise<Array<Array<number>>>(resolve => {
        setTimeout(() => {
            resolve(slotResult);
          }, 1000 + 500 * Math.random());
    });
  }

  informStop(): void {
    const resultRelayed = this.result;
    this.machine.stop(resultRelayed);
  }

  shufflePatterns(): Array<Array<number>> {
    const slotResult = [];
    const randomPattern: number = Math.random();
    let lines:number = 0;

    if(randomPattern < 0.33) { // one line pattern
      lines = 1;
      cc.log('one-line');
    } else if(randomPattern < 0.43) { // two lines pattern
      lines = 2;
      cc.log('two-line');
    } else if(randomPattern < 0.5) { // three lines pattern
      lines = 3;
      cc.log('three-lines');
    } else { // random
      lines = 0;
      cc.log('random');
    }

    let randomLines:Array<boolean> = this.shuffle(lines);

    for (let i = 0; i < randomLines.length; i++) {
      let randomTextureIndex: number = this.getRandomTextureIndex();

      for (let y = 0; y < this.machine.numberOfReels; y++) {
        if(slotResult[y] == null) slotResult[y] = [];

        slotResult[y][i] = randomLines[i] === false ? null : randomTextureIndex;
      }
    }

    return slotResult;
  }

  shuffle(lines): Array<boolean> {
    let randomLines: Array<boolean> = new Array<boolean>(3);

    for(let i = 0; i < randomLines.length; i++) {
      randomLines[i] = i < lines;
    }

    for (let i = randomLines.length - 1; i > 0; i--) {
        let random = Math.floor(Math.random() * (i + 1));
        let temp = randomLines[i];
        randomLines[i] = randomLines[random];
        randomLines[random] = temp;
    }

    return randomLines;
}

  getRandomTextureIndex(): number {
    return Math.floor(Math.random() * this.texturesManager.getTexturesQnty());
  }
}
